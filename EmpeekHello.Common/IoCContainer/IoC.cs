﻿using Microsoft.Practices.Unity;
using System;

namespace EmpeekHello.Common.IoCContainer
{
    public class IoC
    {
        private static IUnityContainer _container;

        static IoC()
        {
            _container = new UnityContainer();
        }

        public static IUnityContainer Container
        {
            get
            {
                return _container;
            }
        }

        public static T Resolve<T>()
        {
            return Container.Resolve<T>();
        }

        public static object Resolve(Type type)
        {
            return Container.Resolve(type);
        }
    }
}
