﻿namespace EmpeekHello.Common
{
    public class OperationRequestResult<T>
    {
        public T Result { get; set; }
        public OperationRequestResultStatus Status { get; set; }
        public string Message { get; set; }
    }
}
