﻿using System.Data.Entity;
using System.Reflection;

namespace EmpeekHello.DataAccess.DataContext
{
    public class DataContext : DbContext, IDataContext
    {
        public DataContext() : base("LocalConnection")
        {

        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            //configurations have been placed in MappingConfigurations folder
            modelBuilder.Configurations.AddFromAssembly(Assembly.GetAssembly(typeof(DataContext)));
        }
    }
}
