﻿using System;
using System.Data.Entity;

namespace EmpeekHello.DataAccess.DataContext
{
    public interface IDataContext : IDisposable
    {
        int SaveChanges();

        DbSet<T> Set<T>() where T : class;
    }
}
