﻿using EmpeekHello.Entities;
using System.Data.Entity.ModelConfiguration;

namespace EmpeekHello.DataAccess.MappingConfigurations
{
    public class ApplicationUserTypeConfiguration : EntityTypeConfiguration<ApplicationUser>
    {
        public ApplicationUserTypeConfiguration()
        {
            this.Property(user => user.RowVersion).IsConcurrencyToken();
        }
    }
}
