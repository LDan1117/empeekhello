﻿using EmpeekHello.Entities;
using System.Data.Entity.ModelConfiguration;

namespace EmpeekHello.DataAccess.MappingConfigurations
{
    public class TransactionTypeConfiguration : EntityTypeConfiguration<Transaction>
    {
        public TransactionTypeConfiguration()
        {
            this.HasRequired(tr => tr.User)
                .WithMany(user => user.Transactions)
                .HasForeignKey(tr => tr.UserId);
        }
    }
}
