namespace EmpeekHello.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class CreateApplicationUsersTable : DbMigration
    {
        public override void Up()
        {
            CreateTable("dbo.ApplicationUsers", builder => new {
                Id = builder.Long(nullable: false, identity: true),
                Name = builder.String(nullable: false, maxLength: 300),
                Password = builder.String(nullable: false),
                Balance = builder.Double(nullable: false),
                RowVersion = builder.Binary()
            })
            .PrimaryKey(u => u.Id);
        }

        public override void Down()
        {
            DropTable("dbo.ApplicationUsers");
        }
    }
}
