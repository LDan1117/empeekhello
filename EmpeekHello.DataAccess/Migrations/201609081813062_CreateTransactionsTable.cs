namespace EmpeekHello.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class CreateTransactionsTable : DbMigration
    {
        public override void Up()
        {
            CreateTable("dbo.Transactions", builder => new {
                Id = builder.Long(nullable: false, identity: true),
                UserId = builder.Long(nullable: false),
                Amount = builder.Double(nullable: false),
                CreateDate = builder.DateTime(nullable: false),
                Type = builder.Int(nullable: false)
            })
            .PrimaryKey(tr => tr.Id);

            AddForeignKey("dbo.Transactions", "UserId", "dbo.ApplicationUsers", "Id");
        }

        public override void Down()
        {
            DropForeignKey("dbo.Transactions", "UserId", "dbo.ApplicationUsers");
            DropTable("dbo.Transactions");
        }
    }
}
