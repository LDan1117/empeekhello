﻿using EmpeekHello.Entities.Base;
using System.Linq;

namespace EmpeekHello.DataAccess.Repositories.Base
{
    public interface IRepository<T> where T : EntityBase
    {
        IQueryable<T> GetAll();

        void Add(T entity);

        void Delete(T entity);

        void SaveChanges();
    }
}
