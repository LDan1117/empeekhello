﻿using EmpeekHello.DataAccess.Repositories.Base;
using EmpeekHello.Entities;
using System.Linq;

namespace EmpeekHello.DataAccess.Repositories
{
    public interface ITransactionRepository : IRepository<Transaction>
    {
        IQueryable<Transaction> GetAllTransactionsForUser(long userId);
    }
}
