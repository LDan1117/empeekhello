﻿using EmpeekHello.DataAccess.Repositories.Base;
using EmpeekHello.Entities;

namespace EmpeekHello.DataAccess.Repositories
{
    public interface IUserRepository : IRepository<ApplicationUser>
    {
        ApplicationUser GetUserById(long userId);
        bool IsExist(long userId);
        bool IsExist(string userName);
    }
}
