﻿using EmpeekHello.DataAccess.Repositories.Base;
using EmpeekHello.Entities;
using System.Linq;
using EmpeekHello.DataAccess.DataContext;

namespace EmpeekHello.DataAccess.Repositories
{
    public class TransactionRepository : Repository<Transaction>, ITransactionRepository
    {
        public TransactionRepository(IDataContext dataContext) : base(dataContext)
        {
        }

        public IQueryable<Transaction> GetAllTransactionsForUser(long userId)
        {
            return this.GetAll().Where(tr => tr.UserId == userId);
        }
    }
}
