﻿using EmpeekHello.DataAccess.Repositories.Base;
using EmpeekHello.Entities;
using System.Linq;
using EmpeekHello.DataAccess.DataContext;
using System;

namespace EmpeekHello.DataAccess.Repositories
{
    public class UserRepository : Repository<ApplicationUser>, IUserRepository
    {
        public UserRepository(IDataContext dataContext) : base(dataContext)
        {
        }

        public ApplicationUser GetUserById(long userId)
        {
            return this.GetAll().FirstOrDefault(u => u.Id == userId);
        }

        public bool IsExist(long userId)
        {
            return this.GetAll().Any(u => u.Id == userId);
        }

        public bool IsExist(string userName)
        {
            return this.GetAll().Any(u => u.Name == userName);
        }
    }
}
