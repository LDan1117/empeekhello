﻿using EmpeekHello.DataAccess.Repositories;
using System;

namespace EmpeekHello.DataAccess.UoW
{
    public interface IUnitOfWork : IDisposable
    {
        IUserRepository Users { get; }
        ITransactionRepository Transactions { get; }

        void SaveChanges();
    }
}
