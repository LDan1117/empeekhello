﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EmpeekHello.DataAccess.UoW
{
    public interface IUnitOfWorkFactory
    {
        IUnitOfWork CreateInstance();
    }
}
