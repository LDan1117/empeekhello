﻿using EmpeekHello.DataAccess.Repositories;
using EmpeekHello.DataAccess.DataContext;

namespace EmpeekHello.DataAccess.UoW
{
    public class UnitOfWork : IUnitOfWork
    {
        private IDataContext _dataContext;
        private IUserRepository _userRepository;
        private ITransactionRepository _transactionRepository;

        public ITransactionRepository Transactions
        {
            get
            {
                if (this._transactionRepository == null)
                {
                    this._transactionRepository = new TransactionRepository(this._dataContext);
                }
                return this._transactionRepository;
            }
        }

        public IUserRepository Users
        {
            get
            {
                if(this._userRepository == null)
                {
                    this._userRepository = new UserRepository(this._dataContext);
                }
                return this._userRepository;
            }
        }

        public UnitOfWork(IDataContext context)
        {
            this._dataContext = context;
        }
        
        public void SaveChanges()
        {
            this._dataContext.SaveChanges();
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    this._dataContext.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
        }
    }
}
