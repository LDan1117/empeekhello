﻿using EmpeekHello.Common.IoCContainer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EmpeekHello.DataAccess.UoW
{
    public class UnitOfWorkFactory : IUnitOfWorkFactory
    {
        public IUnitOfWork CreateInstance()
        {
            return IoC.Resolve<IUnitOfWork>();
        }
    }
}
