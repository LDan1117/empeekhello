﻿using EmpeekHello.Entities.Base;
using System.Collections.Generic;

namespace EmpeekHello.Entities
{
    public class ApplicationUser : EntityBase
    {
        public string Name { get; set; }
        public string Password { get; set; }

        public double Balance { get; set; }

        public byte[] RowVersion { get; set; }

        public virtual ICollection<Transaction> Transactions { get; set; }
    }
}
