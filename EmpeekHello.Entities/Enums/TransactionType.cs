﻿namespace EmpeekHello.Entities
{
    public enum TransactionType
    {
        Deposit,
        Withdrawal,
        Transfer
    }
}
