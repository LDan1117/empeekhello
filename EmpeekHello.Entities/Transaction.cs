﻿using EmpeekHello.Entities.Base;
using System;

namespace EmpeekHello.Entities
{
    public class Transaction : EntityBase
    {
        public long UserId { get; set; }
        public virtual ApplicationUser User { get; set; }

        public double Amount { get; set; }

        public DateTime CreateDate { get; set; }

        public TransactionType Type { get; set; }

        public Transaction()
        {
            CreateDate = DateTime.Now;
        }
    }
}