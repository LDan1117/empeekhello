﻿using EmpeekHello.Services.Interfaces;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using EmpeekHello.Entities;
using EmpeekHello.DataAccess.UoW;
using EmpeekHello.Common;
using System.Data.Entity.Infrastructure;

namespace EmpeekHello.Services.Concrete
{
    public class UserAccountOperationsService : IUserAccountOperationsService
    {
        private IUnitOfWorkFactory _uowFactory;
        private IUserService _userService;

        public UserAccountOperationsService(IUnitOfWorkFactory uowFactory, IUserService userService)
        {
            this._uowFactory = uowFactory;
            this._userService = userService;
        }

        #region IUserAccountOperationsService

        public OperationRequestResult<ApplicationUser> Deposit(long userId, double amount)
        {
            if (userId < 0 || amount < 0)
            {
                return this.Error<ApplicationUser>();
            }

            var user = this._userService.FindUserById(userId);

            if (user == null)
            {
                return this.Error<ApplicationUser>($"The user with id = {userId} doesn't exist.");
            }

            return this.PerfromDepositOperation(user, amount);

        }

        public OperationRequestResult<ApplicationUser> Withdraw(long userId, double amount)
        {
            if (userId < 0 || amount < 0)
            {
                return this.Error<ApplicationUser>();
            }

            var user = this._userService.FindUserById(userId);

            if (user == null)
            {
                return this.Error<ApplicationUser>($"The user with id = {userId} doesn't exist.");
            }            

            return this.PerformWithdrawOperation(user, amount);
        }

        public OperationRequestResult<ApplicationUser> Transfer(long senderId, long receiverId, double amount)
        {
            if (senderId < 0 || receiverId < 0 || amount < 0)
            {
                return this.Error<ApplicationUser>();
            }

            var sender = this._userService.FindUserById(senderId);
            var receiver = this._userService.FindUserById(receiverId);

            if (sender == null || receiver == null)
            {
                return this.Error<ApplicationUser>("The sender or the receiver doesn't exist.");
            }            

            return this.PerformTransferOperation(sender, receiver, amount);            
        }       

        public OperationRequestResult<double> GetUserBalance(long userId)
        {
            if (userId < 0)
            {
                return this.Error<double>();
            }

            var user = this._userService.FindUserById(userId);

            if (user == null)
            {
                return this.Error<double>($"The user with id = {userId} doesn't exist.");
            }

            return this.Success(user.Balance);
        }

        public OperationRequestResult<IEnumerable<Transaction>> GetUserTransactions(long userId)
        {
            if (userId < 0)
            {
                return this.Error<IEnumerable<Transaction>>();
            }

            var user = this._userService.FindUserById(userId);

            if (user == null)
            {
                return this.Error<IEnumerable<Transaction>>($"The user with id = {userId} doesn't exist.");
            }

            return this.Success(user.Transactions.AsEnumerable());
        }

        #endregion

        private OperationRequestResult<T> Success<T>(T result, string message = null)
        {
            return new OperationRequestResult<T>(){
                Status = OperationRequestResultStatus.Success,
                Result = result,
                Message = message
            };
        }

        private OperationRequestResult<T> Error<T>(string message = null)
        {
            return new OperationRequestResult<T>()
            {
                Status = OperationRequestResultStatus.Error,
                Message = message
            };
        }

        private OperationRequestResult<ApplicationUser> PerfromDepositOperation(ApplicationUser user, double amount)
        {
            try
            {
                using (var uow = this._uowFactory.CreateInstance())
                {
                    user.Balance += amount;
                    this.AddTransaction(user, amount, TransactionType.Deposit);

                    uow.SaveChanges();

                    return this.Success(user, "The deposit operation completed successfully.");
                }
            }
            catch (DbUpdateConcurrencyException)
            {
                //get the same user with updated properties
                var updatedUser = this._userService.FindUserById(user.Id);

                //and try again
                return PerformWithdrawOperation(updatedUser, amount);
            }
        }

        private OperationRequestResult<ApplicationUser> PerformWithdrawOperation(ApplicationUser user, double amount)
        {
            try
            {
                using (var uow = this._uowFactory.CreateInstance())
                {
                    if (user.Balance < amount)
                    {
                        return this.Error<ApplicationUser>($"The user with id = {user.Id} doesn't have enough money.");
                    }

                    user.Balance -= amount;
                    this.AddTransaction(user, amount, TransactionType.Withdrawal);

                    uow.SaveChanges();

                    return this.Success(user, "The withdraw operation completed successfully.");
                }
            }
            catch (DbUpdateConcurrencyException)
            {
                //get the same user with updated properties
                var updatedUser = this._userService.FindUserById(user.Id);

                //and try again
                return PerformWithdrawOperation(updatedUser, amount);
            }
        }

        private OperationRequestResult<ApplicationUser> PerformTransferOperation(ApplicationUser from, ApplicationUser to, double amount)
        {
            try
            {
                using (var uow = this._uowFactory.CreateInstance())
                {
                    if (from.Balance < amount)
                    {
                        return this.Error<ApplicationUser>($"The user with id = {from.Id} doesn't have enough money.");
                    }

                    from.Balance -= amount;
                    to.Balance += amount;
                    this.AddTransaction(from, amount, TransactionType.Transfer);

                    uow.SaveChanges();

                    return this.Success(from, "The transfer operation completed successfully.");
                }
            }
            catch (DbUpdateConcurrencyException)
            {
                var updatedSender = this._userService.FindUserById(from.Id);
                var updatedReceiver = this._userService.FindUserById(to.Id);

                return PerformTransferOperation(updatedSender, updatedReceiver, amount);
            }
        }

        private void AddTransaction(ApplicationUser user, double amount, TransactionType type)
        {
            var transaction = new Transaction() {
                User = user,
                Amount = amount,
                Type = type
            };

            user.Transactions.Add(transaction);
        }
    }
}
