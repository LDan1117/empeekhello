﻿using EmpeekHello.Services.Interfaces;
using System;
using System.Linq;
using EmpeekHello.Entities;
using EmpeekHello.DataAccess.UoW;

namespace EmpeekHello.Services.Concrete
{
    public class UserService : IUserService
    {
        private IUnitOfWorkFactory _uowFactory;

        public UserService(IUnitOfWorkFactory uowFactory)
        {
            this._uowFactory = uowFactory;
        }

        public ApplicationUser FindUserById(long userId)
        {
            using (var uow = this._uowFactory.CreateInstance())
            {
                return uow.Users.GetUserById(userId);
            }
        }

        public ApplicationUser FindUserByLogin(string userLogin)
        {
            using (var uow = this._uowFactory.CreateInstance())
            {
                return uow.Users.GetAll().FirstOrDefault(user => user.Name == userLogin);
            }
        }

        public bool IsUserExists(long userId)
        {
            using (var uow = this._uowFactory.CreateInstance())
            {
                return uow.Users.IsExist(userId);
            }
        }
    }
}
