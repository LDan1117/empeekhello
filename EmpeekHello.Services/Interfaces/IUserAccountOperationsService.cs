﻿using EmpeekHello.Common;
using EmpeekHello.Entities;
using System.Collections.Generic;

namespace EmpeekHello.Services.Interfaces
{
    public interface IUserAccountOperationsService
    {
        OperationRequestResult<double> GetUserBalance(long userId);
        OperationRequestResult<IEnumerable<Transaction>> GetUserTransactions(long userId);
        OperationRequestResult<ApplicationUser> Deposit(long userId, double amount);
        OperationRequestResult<ApplicationUser> Withdraw(long userId, double amount);
        OperationRequestResult<ApplicationUser> Transfer(long senderId, long receiverId, double amount);
    }
}
