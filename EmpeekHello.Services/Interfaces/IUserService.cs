﻿using EmpeekHello.Entities;

namespace EmpeekHello.Services.Interfaces
{
    public interface IUserService
    {
        ApplicationUser FindUserById(long userId);
        ApplicationUser FindUserByLogin(string userLogin);
        bool IsUserExists(long userId);
    }
}