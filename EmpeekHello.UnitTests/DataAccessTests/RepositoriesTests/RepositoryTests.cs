﻿using EmpeekHello.DataAccess.DataContext;
using EmpeekHello.DataAccess.Repositories.Base;
using EmpeekHello.UnitTests.Fakes;
using Moq;
using NUnit.Framework;
using System.Data.Entity;
using System.Linq;

namespace EmpeekHello.UnitTests.DataAccessTests.RepositoriesTests
{
    [TestFixture]
    public class RepositoryTests
    {
        #region GetAllTests

        [Test]
        public void GetAll_ReturnsQueryableCollection_FromFakeContext()
        {
            //Arrange
            var fakeSet = FakeDbSetProvider.GetQueryableMockDbSet<EntityMock>();
            var dataContextMock = CreateDataContextMock(fakeSet);

            var repository = CreateTestObject(dataContextMock.Object);

            //Act
            var result = repository.GetAll();

            //Assert
            Assert.AreEqual(fakeSet.AsQueryable(), result);
        }

        #endregion

        #region AddTests

        [Test]
        public void Add_CallsAddMethod_FromFakeDbSet()
        {
            //Arrange
            var fakeSet = new Mock<DbSet<EntityMock>>();
            var dataContextMock = CreateDataContextMock(fakeSet.Object);            

            var repository = CreateTestObject(dataContextMock.Object);

            //Act
            repository.Add(new EntityMock());

            //Assert
            fakeSet.Verify(mock => mock.Add(It.IsAny<EntityMock>()), Times.Once());
        }

        #endregion

        #region DeleteTests

        [Test]
        public void Delete_CallsDeleteMethod_FromFakeDbSet()
        {
            //Arrange
            var fakeSet = new Mock<DbSet<EntityMock>>();
            var dataContextMock = CreateDataContextMock(fakeSet.Object);

            var repository = CreateTestObject(dataContextMock.Object);

            //Act
            repository.Delete(new EntityMock());

            //Assert
            fakeSet.Verify(mock => mock.Remove(It.IsAny<EntityMock>()), Times.Once());
        }

        #endregion

        #region SaveChangesTests

        [Test]
        public void SaveChanges_CallsSaveChangesMethod_FromFakeDataContext()
        {
            //Arrange
            var fakeSet = FakeDbSetProvider.GetQueryableMockDbSet<EntityMock>();
            var dataContextMock = CreateDataContextMock(fakeSet);
            var repository = CreateTestObject(dataContextMock.Object);

            //Act
            repository.SaveChanges();

            //Assert
            dataContextMock.Verify(mock => mock.SaveChanges(), Times.Once());
        }

        #endregion

        #region HelperMethods

        private Repository<EntityMock> CreateTestObject(IDataContext context)
        {
            return new Repository<EntityMock>(context);
        }

        private Mock<IDataContext> CreateDataContextMock(DbSet<EntityMock> set)
        {
            var mock = new Mock<IDataContext>();
            mock.Setup(m => m.Set<EntityMock>()).Returns(set);

            return mock;
        }

        #endregion
    }
}
