﻿using EmpeekHello.DataAccess.DataContext;
using EmpeekHello.DataAccess.Repositories;
using EmpeekHello.Entities;
using EmpeekHello.UnitTests.Fakes;
using Moq;
using NUnit.Framework;
using System.Collections.Generic;
using System.Linq;

namespace EmpeekHello.UnitTests.DataAccessTests.RepositoriesTests
{
    [TestFixture]
    public class TransactionRepositoryTests
    {
        #region GetAllTransactionsForUserTests
        
        [Test]
        public void GetAllTransactionsForUser_WhenSetContains_ReturnsCollectionOfTransactions()
        {
            //Arrange
            var originalCollection = new List<Transaction>() {
                new Transaction() { Id = 1, UserId = 5 },
                new Transaction() { Id = 2, UserId = 5 },
                new Transaction() { Id = 3, UserId = 4 }
            };

            var fakeSet = FakeDbSetProvider.GetQueryableMockDbSet(
                    originalCollection[0], originalCollection[1], originalCollection[2]
                );

            var dataContextMock = new Mock<IDataContext>();
            dataContextMock.Setup(mock => mock.Set<Transaction>()).Returns(fakeSet);

            var repository = CreateTestObject(dataContextMock.Object);

            //Act
            var result = repository.GetAllTransactionsForUser(5);

            //Assert
            Assert.IsTrue(result.All(tr => tr.UserId == 5));
        }

        #endregion

        #region HelperMethods

        private TransactionRepository CreateTestObject()
        {
            return CreateTestObject(new Mock<IDataContext>().Object);
        }

        private TransactionRepository CreateTestObject(IDataContext context)
        {
            return new TransactionRepository(context);
        }

        #endregion
    }
}
