﻿using EmpeekHello.DataAccess.DataContext;
using EmpeekHello.DataAccess.Repositories;
using EmpeekHello.Entities;
using EmpeekHello.UnitTests.Fakes;
using Moq;
using NUnit.Framework;

namespace EmpeekHello.UnitTests.DataAccessTests.RepositoriesTests
{
    [TestFixture]
    public class UserRepositoryTests
    {
        #region GetUserByIdTests

        [Test]
        public void GetUserById_WhenSetContains_ReturnsNeededUser()
        {
            //Arrange
            var expected = new ApplicationUser() { Id = 2, Name = "Pumbaa" };
            var fakeSet = FakeDbSetProvider.GetQueryableMockDbSet(expected);

            var dataContextMock = new Mock<IDataContext>();
            dataContextMock.Setup(mock => mock.Set<ApplicationUser>()).Returns(fakeSet);

            var userRepository = CreateTestObject(dataContextMock.Object);

            //Act
            var result = userRepository.GetUserById(2);

            //Assert
            Assert.AreEqual(expected, result);
        }

        #endregion

        #region IsExistTests

        [Test]
        public void IsNameFree_WhenSetContainsUser_ReturnsTrue()
        {
            //Arrange
            var fakeSet = FakeDbSetProvider.GetQueryableMockDbSet(
                new ApplicationUser() { Id = 1, Name = "Timon" },
                new ApplicationUser() { Id = 2, Name = "Pumbaa" }
                );

            var dataContextMock = new Mock<IDataContext>();
            dataContextMock.Setup(mock => mock.Set<ApplicationUser>()).Returns(fakeSet);

            var userRepository = CreateTestObject(dataContextMock.Object);

            //Act
            var result = userRepository.IsExist("Timon");

            //Assert
            Assert.IsTrue(result);
        }

        [Test]
        public void IsNameFree_WhenSetDoesnotContainUser_ReturnsFalse()
        {
            //Arrange
            var fakeSet = FakeDbSetProvider.GetQueryableMockDbSet(
                new ApplicationUser() { Id = 1, Name = "Timon" },
                new ApplicationUser() { Id = 2, Name = "Pumbaa" }
                );

            var dataContextMock = new Mock<IDataContext>();
            dataContextMock.Setup(mock => mock.Set<ApplicationUser>()).Returns(fakeSet);

            var userRepository = CreateTestObject(dataContextMock.Object);

            //Act
            var result = userRepository.IsExist("xxx_Name_xxx");

            //Assert
            Assert.IsFalse(result);
        }

        #endregion

        #region HelperMethods

        private UserRepository CreateTestObject()
        {
            var mock = new Mock<IDataContext>();

            return CreateTestObject(mock.Object);
        }

        private UserRepository CreateTestObject(IDataContext context)
        {
            return new UserRepository(context);
        }

        #endregion
    }
}
