﻿using EmpeekHello.DataAccess.UoW;
using EmpeekHello.DataAccess.Repositories;
using Moq;
using System;

namespace EmpeekHello.UnitTests.Fakes
{
    public class FakeUnitOfWork : IUnitOfWork
    {
        public Mock<ITransactionRepository> TransactionsMock { get; set; }
        public Mock<IUserRepository> UsersMock { get; set; }

        public IUserRepository Users
        {
            get
            {
                return UsersMock.Object;
            }
        }

        public ITransactionRepository Transactions
        {
            get
            {
                return TransactionsMock.Object;
            }
        }

        public FakeUnitOfWork()
        {
            this.TransactionsMock = new Mock<ITransactionRepository>();
            this.UsersMock = new Mock<IUserRepository>();
        }

        public void Dispose()
        {
        }

        public void SaveChanges()
        {
        }
    }
}
