﻿using EmpeekHello.Common;
using EmpeekHello.DataAccess.UoW;
using EmpeekHello.Entities;
using EmpeekHello.Services.Concrete;
using EmpeekHello.Services.Interfaces;
using EmpeekHello.UnitTests.Fakes;
using Moq;
using NUnit.Framework;
using System.Collections.Generic;
using System.Linq;

namespace EmpeekHello.UnitTests.ServicesTests
{
    [TestFixture]
    public class UserAccountOperationsServiceTests
    {
        #region GetUserBalanceTests
        #endregion

        #region GetUserTransactionsTests
        #endregion

        #region DepositTests

        [Test]
        public void Deposit_WhenReceivesNegativeUserId_ReturnsErrorResult()
        {
            //Arrange
            var service = CreateTestObject();

            //Act
            var result = service.Deposit(-1, 0);

            //Assert
            Assert.AreEqual(OperationRequestResultStatus.Error, result.Status);
            Assert.Null(result.Result);
        }

        [Test]
        public void Deposit_WhenReceivesNegativeAmount_ReturnsErrorResult()
        {
            //Arrange
            var service = CreateTestObject();

            //Act
            var result = service.Deposit(1, -1);

            //Assert
            Assert.AreEqual(OperationRequestResultStatus.Error, result.Status);
            Assert.Null(result.Result);
        }

        [Test]
        public void Deposit_WhenUserDoesnotExist_ReturnsErrorResult()
        {
            //Arrange
            var service = CreateTestObject();

            //Act
            var result = service.Deposit(123123123,1);

            //Asesrt
            Assert.AreEqual(OperationRequestResultStatus.Error, result.Status);
            Assert.Null(result.Result);
        }

        [Test]
        public void Deposit_WhenUserExists_IncreasesItsBalanceAndRecordsNewDepositTransaction()
        {
            //Arrange
            var expectedUser = new ApplicationUser() {
                Id = 1,
                Balance = 12,
                Transactions = new List<Transaction>()
            };

            var uowMock = new Mock<IUnitOfWork>();

            var userServiceMock = new Mock<IUserService>();
            userServiceMock.Setup(mock => mock.FindUserById(It.IsAny<long>())).Returns(expectedUser);

            var service = CreateTestObject(uowMock.Object, userServiceMock.Object);

            //Act
            var result = service.Deposit(1, 35);

            //Assert
            Assert.AreEqual(OperationRequestResultStatus.Success, result.Status);
            Assert.AreSame(expectedUser, result.Result);
            Assert.AreEqual(12 + 35, result.Result.Balance);

            uowMock.Verify(mock => mock.SaveChanges(), Times.Once());
        }

        #endregion

        #region WithdrawTests

        [Test]
        public void Withdraw_WhenReceivesNegativeUserId_ReturnsErrorResult()
        {
            //Arrange
            var service = CreateTestObject();

            //Act
            var result = service.Withdraw(-1,0);

            //Assert
            Assert.AreEqual(OperationRequestResultStatus.Error, result.Status);
            Assert.Null(result.Result);
        }

        [Test]
        public void Withdraw_WhenReceivesNegativeAmount_ReturnsErrorResult()
        {
            //Arrange
            var service = CreateTestObject();

            //Act
            var result = service.Withdraw(1, -1);

            //Assert
            Assert.AreEqual(OperationRequestResultStatus.Error, result.Status);
            Assert.Null(result.Result);
        }

        [Test]
        public void Withdraw_WhenUserDoesnotExist_ReturnsErrorResult()
        {
            //Arrange
            var service = CreateTestObject();

            //Act
            var result = service.Withdraw(123123123, 1);

            //Asesrt
            Assert.AreEqual(OperationRequestResultStatus.Error, result.Status);
            Assert.Null(result.Result);
        }

        [Test]
        public void Withdraw_WhenUserDooesnotHaveEnoughMoney_ReturnsErrorResult()
        {
            //Arrange
            var user = new ApplicationUser() {
                Id = 1,
                Balance = 15
            };

            var uowMock = new Mock<IUnitOfWork>();

            var userServiceMock = new Mock<IUserService>();
            userServiceMock.Setup(mock => mock.FindUserById(It.IsAny<long>())).Returns(user);

            var service = CreateTestObject(uowMock.Object, userServiceMock.Object);

            //Act
            var result = service.Withdraw(1,1000);

            //Assert
            Assert.AreEqual(OperationRequestResultStatus.Error, result.Status);
            Assert.Null(result.Result);

            uowMock.Verify(mock => mock.SaveChanges(), Times.Never());
        }

        [Test]
        public void Withdraw_WhenUserHasMoney_TakesAmountAndRecordsNewTransaction()
        {
            //Arrange
            var user = new ApplicationUser()
            {
                Id = 1,
                Balance = 150,
                Transactions = new List<Transaction>()
            };

            var uowMock = new Mock<IUnitOfWork>();

            var userServiceMock = new Mock<IUserService>();
            userServiceMock.Setup(mock => mock.FindUserById(It.IsAny<long>())).Returns(user);

            var service = CreateTestObject(uowMock.Object, userServiceMock.Object);

            //Act
            var result = service.Withdraw(1,100);

            //Assert
            Assert.AreEqual(OperationRequestResultStatus.Success, result.Status);
            Assert.AreSame(user, result.Result);
            Assert.AreEqual(150 - 100, result.Result.Balance);

            uowMock.Verify(mock => mock.SaveChanges(), Times.Once());
        }

        #endregion

        #region TransferTests

        [Test]
        public void Transfer_WhenReceivesNegativeUserId_ReturnsErrorResult()
        {
            //Arrange
            var service = CreateTestObject();

            //Act
            var result = service.Transfer(-1, 1, 0);

            //Assert
            Assert.AreEqual(OperationRequestResultStatus.Error, result.Status);
            Assert.Null(result.Result);
        }

        [Test]
        public void Transfer_WhenReceivesNegativeReceiverId_ReturnsErrorResult()
        {
            //Arrange
            var service = CreateTestObject();

            //Act
            var result = service.Transfer(1,-1,0);

            //Assert
            Assert.AreEqual(OperationRequestResultStatus.Error, result.Status);
            Assert.Null(result.Result);
        }

        [Test]
        public void Transfer_WhenReceivesNegativeAmount_ReturnsErrorResult()
        {
            //Arrange
            var service = CreateTestObject();

            //Act
            var result = service.Transfer(1,2,-1);

            //Assert
            Assert.AreEqual(OperationRequestResultStatus.Error, result.Status);
            Assert.Null(result.Result);
        }

        [Test]
        public void Transfer_WhenUserDoesnotExist_ReturnsErrorResult()
        {
            //Arrange
            var service = CreateTestObject();

            //Act
            var result = service.Transfer(123123123,1,0);

            //Asesrt
            Assert.AreEqual(OperationRequestResultStatus.Error, result.Status);
            Assert.Null(result.Result);
        }

        [Test]
        public void Transfer_WhenReceiverDoesnotExist_ReturnsErrorResult()
        {
            //Arrange
            var uowMock = new Mock<IUnitOfWork>();

            var userServiceMock = new Mock<IUserService>();
            userServiceMock.Setup(mock => mock.FindUserById(It.Is<long>(id => id == 1))).Returns(new ApplicationUser());

            var service = CreateTestObject(uowMock.Object, userServiceMock.Object);

            //Act
            var result = service.Transfer(1,1276567,0);

            //Asesrt
            Assert.AreEqual(OperationRequestResultStatus.Error, result.Status);
            Assert.Null(result.Result);

            uowMock.Verify(mock => mock.SaveChanges(), Times.Never());
        }

        [Test]
        public void Transfer_WhenUserDooesnotHaveEnoughMoney_ReturnsErrorResult()
        {
            //Arrange
            var user1 = new ApplicationUser()
            {
                Id = 1,
                Balance = 15
            };

            var user2 = new ApplicationUser()
            {
                Id = 2,
                Balance = 31
            };

            var uowMock = new Mock<IUnitOfWork>();

            var userServiceMock = new Mock<IUserService>();
            userServiceMock
                .SetupSequence(mock => mock.FindUserById(It.IsAny<long>()))
                .Returns(user1)
                .Returns(user2);

            var service = CreateTestObject(uowMock.Object, userServiceMock.Object);

            //Act
            var result = service.Transfer(1,2,1000);

            //Assert
            Assert.AreEqual(OperationRequestResultStatus.Error, result.Status);
            Assert.Null(result.Result);

            uowMock.Verify(mock => mock.SaveChanges(), Times.Never());
        }

        [Test]
        public void Transfer_WhenUserHasMoney_TakesItsMoneyAndIncreaseReceiversBalanceThenRecordsNewTransaction()
        {
            //Arrange
            var user1 = new ApplicationUser()
            {
                Id = 1,
                Balance = 150,
                Transactions = new List<Transaction>()
            };

            var user2 = new ApplicationUser()
            {
                Id = 2,
                Balance = 15,
            };

            var uowMock = new Mock<IUnitOfWork>();

            var userServiceMock = new Mock<IUserService>();
            userServiceMock
                .SetupSequence(mock => mock.FindUserById(It.IsAny<long>()))
                .Returns(user1)
                .Returns(user2);

            var service = CreateTestObject(uowMock.Object, userServiceMock.Object);

            //Act
            var result = service.Transfer(1,2,100);

            //Assert
            Assert.AreEqual(OperationRequestResultStatus.Success, result.Status);
            Assert.AreSame(user1, result.Result);
            Assert.AreEqual(150 - 100, result.Result.Balance);
            Assert.AreEqual(15 + 100, user2.Balance);

            uowMock.Verify(mock => mock.SaveChanges(), Times.Once());
        }

        #endregion

        #region GetUserBalanceTest

        [Test]
        public void GetUserBalance_WhenReceivesNegativeUserId_ReturnsErrorResult()
        {
            //Arrange
            var service = CreateTestObject();

            //Act
            var result = service.GetUserBalance(-1);

            //Assert
            Assert.AreEqual(OperationRequestResultStatus.Error, result.Status);
            Assert.AreEqual(0, result.Result);
        }

        [Test]
        public void GetUserBalance_WhenUserDoesnotExist_ReturnsErrorResult()
        {
            //Arrange
            var service = CreateTestObject();

            //Act
            var result = service.GetUserBalance(23131354);

            //Asesrt
            Assert.AreEqual(OperationRequestResultStatus.Error, result.Status);
            Assert.AreEqual(0, result.Result);
        }

        [Test]
        public void GetUserBalance_WhenUserExists_ReturnsSuccessResultWithUserBalance()
        {
            //Arrange
            var user = new ApplicationUser(){
                Id = 1,
                Balance = 123
            };

            var userServiceMock = new Mock<IUserService>();
            userServiceMock.Setup(mock => mock.FindUserById(It.IsAny<long>())).Returns(user);

            var service = CreateTestObject(userServiceMock.Object);

            //Act
            var result = service.GetUserBalance(1);

            //Asesrt
            Assert.AreEqual(OperationRequestResultStatus.Success, result.Status);
            Assert.AreEqual(123, result.Result);
        }

        #endregion

        #region GetUserTransactionsTests

        [Test]
        public void GetUserTransactions_WhenReceivesNegativeUserId_ReturnsErrorResult()
        {
            //Arrange
            var service = CreateTestObject();

            //Act
            var result = service.GetUserTransactions(-1);

            //Assert
            Assert.AreEqual(OperationRequestResultStatus.Error, result.Status);
            Assert.Null(result.Result);
        }

        [Test]
        public void GetUserTransactions_WhenUserDoesnotExist_ReturnsErrorResult()
        {
            //Arrange
            var service = CreateTestObject();

            //Act
            var result = service.GetUserTransactions(4323);

            //Asesrt
            Assert.AreEqual(OperationRequestResultStatus.Error, result.Status);
            Assert.Null(result.Result);
        }

        [Test]
        public void GetUserTransactions_WhenUserExists_ReturnsSuccessResultWithAllUserTransactions()
        {
            //Arrange
            var user = new ApplicationUser() {
                Id = 1,
                Transactions = new List<Transaction>() {
                    new Transaction() { Id = 1, UserId = 1 },
                    new Transaction() { Id = 2, UserId = 1 },
                    new Transaction() { Id = 3, UserId = 1 },
                }
            };

            var userServiceMock = new Mock<IUserService>();
            userServiceMock.Setup(mock => mock.FindUserById(It.IsAny<long>())).Returns(user);

            var service = CreateTestObject(userServiceMock.Object);

            //Act
            var result = service.GetUserTransactions(1);

            //Assert
            Assert.AreEqual(OperationRequestResultStatus.Success, result.Status);
            Assert.AreEqual(user.Transactions.AsEnumerable(), result.Result);
        }

        #endregion

        #region HelperMethods

        private UserAccountOperationsService CreateTestObject()
        {
            var uowMock = new Mock<IUnitOfWork>();
            var uowFactoryMock = new Mock<IUnitOfWorkFactory>();
            uowFactoryMock.Setup(mock => mock.CreateInstance()).Returns(uowMock.Object);

            var userServiceMock = new Mock<IUserService>();

            return new UserAccountOperationsService(uowFactoryMock.Object, userServiceMock.Object);
        }

        private UserAccountOperationsService CreateTestObject(IUserService userService)
        {
            var uowMock = new Mock<IUnitOfWork>();
            var uowFactoryMock = new Mock<IUnitOfWorkFactory>();
            uowFactoryMock.Setup(mock => mock.CreateInstance()).Returns(uowMock.Object);

            return new UserAccountOperationsService(uowFactoryMock.Object, userService);
        }

        private UserAccountOperationsService CreateTestObject(IUnitOfWork uow, IUserService userService)
        {
            var uowFactoryMock = new Mock<IUnitOfWorkFactory>();
            uowFactoryMock.Setup(mock => mock.CreateInstance()).Returns(uow);

            return new UserAccountOperationsService(uowFactoryMock.Object, userService);
        }

        private UserAccountOperationsService CreateTestObject(IUnitOfWorkFactory uowFactory, IUserService userService)
        {
            return new UserAccountOperationsService(uowFactory, userService);
        }

        #endregion
    }
}
