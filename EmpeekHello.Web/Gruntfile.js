﻿module.exports = function (grunt) {

    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-changed');

    //grunt.loadNpmTasks('grunt-contrib-watch');  ???

    grunt.initConfig({
        uglify: {
            all: {
                files: {
                    'Assets/js/app.js': [] //'Scripts/app.js', 'Scripts/**/*.js'
                }
            }
        },

        //watch: {
        //    scripts: {
        //        files: ['/*.js'],
        //        tasks: ['uglify']
        //    }
        //}
    });

    grunt.registerTask('minify', ['changed:uglify:all']);
};